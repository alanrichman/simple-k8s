# Simple K8s

This is a simple 2 container Python web app. I am aiming for the development of this to aid in teaching me how to use Kubernetes. This app will probably never really *do* anything, but maybe in the future it can serve others as an easy-to-reference K8s project which demonstrates usage of some of the basic primitives.

## Running Locally

I am using the Kubernetes feature built into Docker Desktop. I tried using `minikube` first, but ran into issues with ingress. The computer I am writing on is Windows 10 Home which means that it does not have Hyper-V. When I used the Docker VM Driver I ran into more issues with ingress, and apparently this is a known limitation. I tried installing Virtual Box but it ran into conflicts with Docker. Finally I found out that Docker Desktop has a Kubernetes cluster setting so I used that, and it was turn-key.

### Seeding the database

The backend expects a table to exist in the database by the name of `person` with the following high-level structure:

| Field Name | Type     | Parameters                                 |
| :--------- | :------- | :----------------------------------------- |
| `id`       | `int`    | `PRIMARY_KEY`, `NOT NULL`, `AUTOINCREMENT` |
| `name`     | `string` | `NOT NULL`                                 |
| `color`    | `string` | `NOT NULL`                                 |

This can be created using the `db-seed.sql` script provided or by using a GUI like [`dbgate`](https://dbgate.org/), which is what I did.

### DNS

You should create an entry in your `hosts` file like sucH

`127.0.0.1 simple-k8s.local`

On Windows this file is `C:\Windows\System32\drivers\etc\hosts`, on Linux it is `/etc/hosts`

## Updating

I am using Windows 10 and PowerShell (fight me) so I wrote some very basic PS1 scripts to store the `kubectl` and `docker` commands due to Lazy™. Buy-and-large they could be copied to Linux shell scripts with nearly no changes. 

- Rebuild and push the container images with `scripts\build-docker.ps1`
- Deploy to the cluster for the first time with `scripts\build-cluster.ps1`
- Update the cluster with the new definitions with `scripts\apply-cluster.ps1`

A fun exercise to see how Kubernetes is working under the hood is the refresh the page a few times right after running `kubectl apply` with a new container image version. You can see that some of the replicas are still on the old version of the old version while some are on the new. Gradually they all change to the new one. Paired with having the output of the webpage contain the pod name (just for learning purposes) it lets you have some insight into that process.
