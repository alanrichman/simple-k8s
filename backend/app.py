import flask
import json
import os
import psycopg2

app = flask.Flask(__name__)

connection = psycopg2.connect(
    database=os.environ.get("POSTGRES_DB"),
    user="postgres",
    password=os.environ.get("POSTGRES_PASSWORD"),
    host=os.environ.get("DB_HOST"),
    port="5432"
)

@app.route("/")
def root():
    people = []
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM person")
    records = cursor.fetchall()
    for record in records:
        people.append({
            "firstname": record[1],
            "color": record[2]
        })
    
    return json.dumps({"people": people, "backend": os.environ.get("HOSTNAME", "NOKUBE")})

if __name__ == "__main__":
    app.run()
