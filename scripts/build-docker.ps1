$tag = (Get-Date -Format "yyyymmddHHmmssffff")

docker build --file backend/Dockerfile --tag alanrichman/simple-k8s-backend:$tag ./backend/
docker push alanrichman/simple-k8s-backend:$tag

docker build --file frontend/Dockerfile --tag alanrichman/simple-k8s-frontend:$tag ./frontend/
docker push alanrichman/simple-k8s-frontend:$tag
