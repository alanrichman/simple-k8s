CREATE TABLE "person" ( 
  "id" SERIAL,
  "name" VARCHAR(250) NOT NULL,
  "color" VARCHAR(250) NOT NULL,
   PRIMARY KEY ("id")
);

INSERT INTO "public"."person" ("name", "color") VALUES ("Alan", "Purple");
INSERT INTO "public"."person" ("name", "color") VALUES ("Madi", "Green");
