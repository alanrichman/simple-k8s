import flask
import requests
import os

app = flask.Flask(__name__)

@app.route("/")
def root():
    response = requests.get(os.environ.get("API_URL"))
    return flask.render_template(
        "index.html",
        people=response.json()["people"],
        backend=response.json()["backend"],
        frontend=os.environ.get("HOSTNAME", "NOKUBE")
    )

if __name__ == "__main__":
    app.run()
